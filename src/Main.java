public class Main {

    public static void main(String[] args) {

        Employee emp = new Employee(1,"Ram");
        System.out.println(String.format("emp: %s , Calculated bonus , %.2f ", emp,emp.calculateBonus(30000.00)) );

    }
}
